#!/usr/bin/env bash
#Description: Push changes to repository
#Author: SysAdTrainee (toro.exavierace.munar@gmail.com)
  #read -p "File name: " file
  read -p "Commit description: " desc
  read -p "Branch name: " bnch
  git add -A
  git commit -m "$desc"
  git push origin $bnch
